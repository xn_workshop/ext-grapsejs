# GrapesJS Editor extension for laravel-admin

This is a `laravel-admin` extension that integrates [GrapesJS Editor](https://github.com/josdejong/jsoneditor) into `laravel-admin`.

[DEMO](http://demo.laravel-admin.org/editors/json) Login using `admin/admin`


## Installation

```bash
composer require xn/grapesjs-editor
php artisan vendor:publish --tag=laravel-admin-grapesjs-editor
```

## Update
```bash
composer require xn/grapesjs-editor
php artisan vendor:publish --tag=laravel-admin-grapesjs-editor --force
```

## Configuration

when content with js-script needs to disablePjax in Controller edit method
```php
/**
    * Edit interface.
    *
    * @param mixed   $id
    * @param Content $content
    *
    * @return Content
    */
public function edit($id, Content $content)
{
    Admin::disablePjax();
    return parent::edit($id, $content);
}
```
In the `extensions` section of the `config/admin.php` file, add some configuration that belongs to this extension.
```php
'extensions' => [
    'grapesjs-editor' => [
        // set to false if you want to disable this extension
        'enable' => true,
        'config' => [
            'styles' => [
                "//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css",
            ],
            'scripts' => [
                "//code.jquery.com/jquery-3.6.4.min.js",
                "//cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            ]
        ],
        'plugins' => [
            'styles' => [
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/css/grapesjs-preset-webpage.min.css',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/css/tooltip.css',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/css/grapesjs-component-code-editor.min.css',
            ],
            'scripts' => [
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-preset-webpage.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/gjs-blocks-basic.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-plugin-forms.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-component-countdown.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-plugin-export.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-tabs.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-custom-code.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-touch.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-parser-postcss.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-tooltip.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-tui-image-editor.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-typed.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-style-bg.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-component-code-editor.js',
                '//cdn.ckeditor.com/4.20.1/full-all/ckeditor.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-plugin-ckeditor.min.js',
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/grapesjs-rulers.min.js',
                //
                'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/plugins-opts.js',
            ]
        ],
    ]
]
```

More configurations can be found at [GrapesJS Editor](https://github.com/josdejong/grapesjseditor).

## Usage

Use it in the form form:
```php
$form->grapesjs('content')->options(['height' => 500])->setWidth(12, 0);
```

## More resources

[Awesome Laravel-admin](https://github.com/jxlwqq/awesome-laravel-admin)

## License

Licensed under [The MIT License](LICENSE).
