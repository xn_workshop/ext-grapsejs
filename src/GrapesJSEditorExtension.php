<?php

namespace Xn\GrapesJSEditor;

use Xn\Admin\Extension;

class GrapesJSEditorExtension extends Extension
{
    public $name = "laravel-admin-grapesjs-editor";

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';
}
