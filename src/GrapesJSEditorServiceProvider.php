<?php

namespace Xn\GrapesJSEditor;

use Xn\Admin\Admin;
use Xn\Admin\Form;
use Illuminate\Support\ServiceProvider;

class GrapesJSEditorServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(GrapesJSEditorExtension $extension)
    {
        if (! GrapesJSEditorExtension::boot()) {
            return ;
        }

        $this->loadViewsFrom(resource_path('views')."/vendor/xn/grapesjs-editor/views", $extension->name);

        $this->registerPublishing($extension);

        Admin::booting(function () {
            Form::extend('grapesjs', Editor::class);
        });
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing(GrapesJSEditorExtension $extension)
    {
        if ($this->app->runningInConsole()) {
            $views = $extension->views();
            $assets = $extension->assets();
            $this->publishes(
                [
                    $views => resource_path('views')."/vendor/xn/grapesjs-editor/views",
                    $assets => public_path('vendor/laravel-admin-ext/grapesjs-editor')
                ], 
                $extension->name
            );
        }
    }
}
