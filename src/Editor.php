<?php

namespace Xn\GrapesJSEditor;

use Xn\Admin\Form\Field;
use Xn\Admin\Form\NestedForm;

class Editor extends Field
{
    protected $view = 'laravel-admin-grapesjs-editor::editor';

    protected static $css = [
        'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/dist/css/grapes.min.css',
    ];

    protected static $js = [
        'vendor/laravel-admin-ext/grapesjs-editor/grapesjs/dist/grapes.min.js',
    ];

    /**
     * Get assets required by this field.
     *
     * @return array
     */
    public static function getAssets()
    {
        $plugins = config('admin.extensions.grapesjs-editor.plugins');

        static::$css = array_merge(static::$css, $plugins['styles']);

        static::$js = array_merge(static::$js, $plugins['scripts']);

        return [
            'css' => static::$css,
            'js'  => static::$js,
        ];
    }

    protected function plugins()
    {
        $plugins = [];
        foreach(static::$js as $js){
            $_js = explode("/", $js);
            if (strpos(end($_js), 'grapesjs') !== false
            || strpos(end($_js), 'gjs') !== false) {
                $plugins[] = current(explode(".", end($_js)));
            }
        }
        return $plugins;
    }

    public function render()
    {

        $html = old($this->column, $this->value());

        $plugins = json_encode($this->plugins());

        $this->value = $html;

        $options = json_encode(config('admin.extensions.grapesjs-editor.config'));

        $height = $this->options['height']??'100vh';

        $name = $this->variables()["name"];
        $_name = str_replace(['[',']'], '_',$name);
        $defaultKey = NestedForm::DEFAULT_KEY_NAME;
        $this->script = <<<EOT

$(function(){
    // create the editor
    var name="$name";
    var _name="$_name";
    try {
        name="$name".replace(/{$defaultKey}/g, index);
        _name="$_name".replace(/{$defaultKey}/g, index);
    }catch(e){
    }
    var _container = document.getElementById(name);
    var plp = '//via.placeholder.com/350x250/';
    var images = [
        plp + '78c5d6/fff',
        plp + '459ba8/fff',
        plp + '79c267/fff',
        plp + 'c5d647/fff',
        plp + 'f28c33/fff',
        plp + 'e868a2/fff',
        plp + 'cc4360/fff',
      ];
    window.editor = grapesjs.init({
        canvas: $options,
        // Indicate where to init the editor. You can also pass an HTMLElement
        container: _container,
        // Get the content for the canvas directly from the element
        // As an alternative we could use: `components: '<h1>Hello World Component!</h1>'`,
        fromElement: true,
        allowScripts: true,
        // Size of the editor
        height: '$height',
        width: 'auto',
        // Disable the storage manager for the moment
        storageManager: false,
        showOffsets: true,
        assetManager: {
          embedAsBase64: true,
          assets: images
        },
        i18n: _i18n,
        selectorManager: { componentFirst: true },
        styleManager: _styleManager,
        plugins: $plugins,
        pluginsOpts: _pluginsOpts,
    });

    editor.on('load', (gjs, argument) => {
        url = '/vendor/laravel-admin-ext/grapesjs-editor/grapesjs/plugins/plugins-custom.js';
        var script = document.createElement('script');
        script.onload = function () {
            console.log('custom gjs loaded');
        };
        script.src = url;
        document.head.appendChild(script);
    });

    $('form').on('submit', function(){
        $('input[name="'+name+'"]').val(editor.getHtml() + '<style>'+editor.getCss()+'</style>');
        return true;
    });
});

EOT;

        return parent::render();
    }
}
